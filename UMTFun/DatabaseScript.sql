﻿USE UMTFun;
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
               WHERE TABLE_NAME = N'Users')
	CREATE TABLE [Users]
	(
	UserID UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(100) NOT NULL,
	[Account] NVARCHAR(100) NOT NULL UNIQUE
	);
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'Categories')
BEGIN
	CREATE TABLE [Categories]
	(
	CategoryID UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(255)
	);

	INSERT INTO Categories(CategoryID, Name) VALUES (NEWID(), N'Cookies');
	INSERT INTO Categories(CategoryID, Name) VALUES (NEWID(), N'Food Order');
	INSERT INTO Categories(CategoryID, Name) VALUES (NEWID(), N'Teambuilding');
	INSERT INTO Categories(CategoryID, Name) VALUES (NEWID(), N'Drinkbuilding');
	INSERT INTO Categories(CategoryID, Name) VALUES (NEWID(), N'Football');
	INSERT INTO Categories(CategoryID, Name) VALUES (NEWID(), N'Custom Event');
END
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
               WHERE TABLE_NAME = N'Events')
	CREATE TABLE [Events]
	(
	EventID UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	CategoryID UNIQUEIDENTIFIER NOT NULL,
	[Subject] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(255),
	CreatedBy UNIQUEIDENTIFIER NOT NULL,
	Deadline datetime NOT NULL,
	LastModifiedDate datetime NOT NULL,
	CONSTRAINT fk_EventCategory FOREIGN KEY (CategoryID) REFERENCES [Categories](CategoryID)
	);
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'EventUserMappings')
	CREATE TABLE [EventUserMappings]
	(
	EventID UNIQUEIDENTIFIER NOT NULL,
	UserID UNIQUEIDENTIFIER NOT NULL,
	PRIMARY KEY (EventID, UserID),
	CONSTRAINT fk_EventMapping FOREIGN KEY (EventID) REFERENCES [Events](EventID),
	CONSTRAINT fk_UserMapping FOREIGN KEY (UserID) REFERENCES [Users](UserID)
	);
GO

IF EXISTS (SELECT * FROM sys.objects
           WHERE object_id = OBJECT_ID(N'InsertUser') AND type IN ( N'P', N'PC' ) )
DROP PROCEDURE InsertUser
GO

CREATE PROCEDURE InsertUser
(
@name NVARCHAR(100),
@account NVARCHAR(100)
)
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [Users] WHERE Account = @account)
		INSERT INTO [Users] (UserID, Name, Account)
		VALUES (NEWID(), @name, @account);
END
GO

IF EXISTS (SELECT * FROM sys.objects
           WHERE object_id = OBJECT_ID(N'ReadCategories') AND type IN ( N'P', N'PC' ) )
DROP PROCEDURE ReadCategories
GO

CREATE PROCEDURE ReadCategories
AS
BEGIN
	SELECT CategoryID, Name, [Description]
	FROM Categories
END
GO