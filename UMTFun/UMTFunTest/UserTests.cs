﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UMTFunTest.FunService;
using System.DirectoryServices.AccountManagement;

namespace UMTFunTest
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void RegisterUser()
        {
            using (FunClient fun = new FunClient())
            {
                Response response = fun.RegisterUser(new User { Account = string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName), Name = UserPrincipal.Current.DisplayName });

                Assert.AreEqual(0, response.ErrorCode);
            }
        }
    }
}
