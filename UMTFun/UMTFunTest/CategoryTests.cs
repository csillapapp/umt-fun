﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UMTFunTest.FunService;

namespace UMTFunTest
{
    [TestClass]
    public class CategoryTests
    {
        [TestMethod]
        public void GetCategories()
        {
            using (FunClient fun = new FunClient())
            {
                var response = fun.GetCategories();

                Assert.AreEqual(0, response.ErrorCode);
                Assert.IsNotNull(response.Data);
            }
        }
    }
}
