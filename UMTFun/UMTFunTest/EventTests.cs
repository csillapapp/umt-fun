﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UMTFunTest.FunService;

namespace UMTFunTest
{
    [TestClass]
    public class EventTests
    {
        [TestMethod]
        public void CreateEvent()
        {
            using (FunClient fun = new FunClient())
            {
                var categories = fun.GetCategories().Data;

                Response response = fun.CreateEvent(new Event
                {
                    Category = new Category { ID = categories.First().ID },
                    CreatedBy = new User { Account = string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName) },
                    Deadline = DateTime.Now.AddDays(1),
                    Description = "test description",
                    Subject = "test subject"
                });

                Assert.AreEqual(0, response.ErrorCode);
            }
        }

        [TestMethod]
        public void CreateEventInvalidCategory()
        {
            using (FunClient fun = new FunClient())
            {
                Response response = fun.CreateEvent(new Event
                {
                    Category = new Category { ID = Guid.NewGuid() },
                    CreatedBy = new User { Account = string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName) },
                    Deadline = DateTime.Now.AddDays(1),
                    Description = "test description",
                    Subject = "test subject"
                });

                Assert.AreEqual(53000, response.ErrorCode);
            }
        }

        [TestMethod]
        public void CreateEventInvalidUser()
        {
            using (FunClient fun = new FunClient())
            {
                var categories = fun.GetCategories().Data;

                Response response = fun.CreateEvent(new Event
                {
                    Category = new Category { ID = categories.First().ID },
                    CreatedBy = new User { Account = "lalala" },
                    Deadline = DateTime.Now.AddDays(1),
                    Description = "test description",
                    Subject = "test subject"
                });

                Assert.AreEqual(54000, response.ErrorCode);
            }
        }
    }
}
