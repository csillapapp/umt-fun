﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using UMTFunService.Model;

namespace UMTFunService
{
    [ServiceContract]
    public interface IFun
    {
        [OperationContract]
        Response RegisterUser(User user);

        [OperationContract]
        DataResponse<List<Category>> GetCategories();

        [OperationContract]
        Response CreateEvent(Event newEvent);

        [OperationContract]
        Response ModifyEvent(Event modifiedEvent);

        [OperationContract]
        Response DeleteEvent(Guid eventID);

        [OperationContract]
        DataResponse<List<Event>> GetEvents(EventFilter filter);

        [OperationContract]
        DataResponse<Event> GetEventDetails(Guid eventID);
    }
}
