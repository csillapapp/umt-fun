﻿using System;
using System.Data.SqlClient;
using UMTFunService.BLL;
using UMTFunService.Model;

namespace UMTFunService
{
    public class ResponseHelper
    {
        public static DataResponse<T> BuildDataResponse<T>(Func<T> function)
        {
            DataResponse<T> response = new DataResponse<T>();

            try
            {
                response.Data = function();
                response.ErrorCode = (int)ErrorCodes.Success;
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Errors.Count > 0)
                    response.ErrorCode = sqlex.Errors[0].Number;
                else
                    response.ErrorCode = (int)ErrorCodes.UnknownError;

                Errors.LogError(sqlex);
            }
            catch (Exception ex)
            {
                response.ErrorCode = (int)ErrorCodes.UnknownError;
                Errors.LogError(ex);
            }

            return response;
        }

        public static Response BuildResponse(Action action)
        {
            Response response = new Response();

            try
            {
                action();
                response.ErrorCode = (int)ErrorCodes.Success;
            }
            catch (SqlException sqlex)
            {
                if (sqlex.Errors.Count > 0)
                    response.ErrorCode = sqlex.Errors[0].Number;
                else
                    response.ErrorCode = (int)ErrorCodes.UnknownError;

                Errors.LogError(sqlex);
            }
            catch (Exception ex)
            {
                response.ErrorCode = (int)ErrorCodes.UnknownError;
                Errors.LogError(ex);
            }

            return response;
        }
    }
}