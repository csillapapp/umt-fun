﻿using System;
using System.Collections.Generic;
using UMTFunService.BLL;
using UMTFunService.Model;

namespace UMTFunService
{
    public class Fun : IFun
    {
        public Response RegisterUser(User user)
        {
            return ResponseHelper.BuildResponse(() => 
            {
                new Users().RegisterUser(user);
            });
        }

        public DataResponse<List<Category>> GetCategories()
        {
            return ResponseHelper.BuildDataResponse(() => 
            {
                return new Categories().GetCategories();
            });
        }

        public Response CreateEvent(Event newEvent)
        {
            return ResponseHelper.BuildResponse(() => 
            {
                new Events().CreateEvent(newEvent);
            });
        }

        public Response ModifyEvent(Event modifiedEvent)
        {
            return new Response();
        }

        public Response DeleteEvent(Guid eventID)
        {
            return new Response();
        }

        public DataResponse<List<Event>> GetEvents(EventFilter filter)
        {
            return new DataResponse<List<Event>>();
        }

        public DataResponse<Event> GetEventDetails(Guid eventID)
        {
            return new DataResponse<Event>();
        }
    }
}
