﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace UMTFunService.DAL
{
    public class Database
    {
        static readonly string ConnectionString;

        static Database()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
        }

        public void AddParameter(SqlCommand command, string parameterName, object parameterValue)
        {
            command.Parameters.Add(new SqlParameter(parameterName, parameterValue));
        }

        public SqlCommand GetStoredProcedureCommand(string commandText)
        {
            SqlCommand command = new SqlCommand(commandText);
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = new SqlConnection(ConnectionString);

            return command;
        }

        public DataTable ExecuteQuery(SqlCommand command)
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                DataTable table = new DataTable();
                adapter.Fill(table);

                return table;
            }
        }

        public void ExecuteNonQuery(SqlCommand command)
        {
            SqlConnection connection = command.Connection ?? new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }
    }
}