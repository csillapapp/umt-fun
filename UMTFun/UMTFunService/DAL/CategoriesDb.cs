﻿using System.Data;
using System.Data.SqlClient;

namespace UMTFunService.DAL
{
    public class CategoriesDb
    {
        const string GetCategoriesSP = "ReadCategories";

        internal DataTable GetCategories()
        {
            Database database = new Database();
            SqlCommand command = database.GetStoredProcedureCommand(GetCategoriesSP);

            return database.ExecuteQuery(command);
        }
    }
}