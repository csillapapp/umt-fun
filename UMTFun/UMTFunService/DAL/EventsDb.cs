﻿using System.Data.SqlClient;
using UMTFunService.Model;

namespace UMTFunService.DAL
{
    public class EventsDb
    {
        const string CreateEventSP = "CreateEvent";

        internal void CreateEvent(Event newEvent)
        {
            Database database = new Database();
            SqlCommand command = database.GetStoredProcedureCommand(CreateEventSP);
            database.AddParameter(command, "@categoryID", newEvent.Category.ID);
            database.AddParameter(command, "@subject", newEvent.Subject);
            database.AddParameter(command, "@description", newEvent.Description);
            database.AddParameter(command, "@account", newEvent.CreatedBy.Account);
            database.AddParameter(command, "@deadline", newEvent.Deadline);

            database.ExecuteNonQuery(command);
        }
    }
}