﻿using System.Data.SqlClient;
using UMTFunService.Model;

namespace UMTFunService.DAL
{
    public class UsersDb
    {
        const string RegisterUserSP = "InsertUsers";

        internal void RegisterUser(User user)
        {
            Database database = new Database();
            using (SqlCommand command = database.GetStoredProcedureCommand(RegisterUserSP))
            {
                database.AddParameter(command, "@name", user.Name);
                database.AddParameter(command, "account", user.Account);
                database.ExecuteNonQuery(command);
            }
        }
    }
}