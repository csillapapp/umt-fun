﻿using System.Runtime.Serialization;

namespace UMTFunService.Model
{
    [DataContract]
    public class Response
    {
        [DataMember]
        public int ErrorCode { get; set; }
    }

    public enum ErrorCodes
    {
        Success = 0,
        UnknownError = 1
    }
}