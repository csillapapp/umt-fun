﻿using System;
using System.Runtime.Serialization;

namespace UMTFunService.Model
{
    [DataContract]
    public class Event
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public Category Category { get; set; }

        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public User CreatedBy { get; set; }

        [DataMember]
        public DateTime LastModifiedDate { get; set; }

        [DataMember]
        public DateTime Deadline { get; set; }
    }
}