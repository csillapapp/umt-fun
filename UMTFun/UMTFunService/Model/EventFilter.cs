﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UMTFunService.Model
{
    [DataContract]
    public class EventFilter
    {
        [DataMember]
        public List<Guid> CategorieIDs { get; set; }

        [DataMember]
        public string Account { get; set; }
    }
}