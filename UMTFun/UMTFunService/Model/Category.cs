﻿using System;
using System.Runtime.Serialization;

namespace UMTFunService.Model
{
    [DataContract]
    public class Category
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}