﻿using System;
using System.Runtime.Serialization;

namespace UMTFunService.Model
{
    [DataContract]
    public class User
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Account { get; set; }
    }
}