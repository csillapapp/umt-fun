﻿using System.Runtime.Serialization;

namespace UMTFunService.Model
{
    [DataContract]
    public class DataResponse<T> : Response
    {
        [DataMember]
        public T Data { get; set; }
    }
}