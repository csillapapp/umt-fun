﻿using System;
using System.Collections.Generic;
using System.Data;
using UMTFunService.DAL;
using UMTFunService.Model;

namespace UMTFunService.BLL
{
    public class Categories
    {
        CategoriesDb categoriesDb;

        public Categories()
        {
            categoriesDb = new CategoriesDb();
        }

        public List<Category> GetCategories()
        {
            using (DataTable table = categoriesDb.GetCategories())
            {
                List<Category> categories = new List<Category>();

                foreach (DataRow row in table.Rows)
                {
                    Category category = new Category();

                    category.ID = (Guid)row["CategoryID"];
                    category.Name = row["Name"].ToString();
                    category.Description = row["Description"].ToString();

                    categories.Add(category);
                }

                return categories;
            }
        }
    }
}