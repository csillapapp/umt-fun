﻿using System;
using UMTFunService.DAL;

namespace UMTFunService.BLL
{
    public class Errors
    {
        public static void LogError(Exception ex)
        {
            ErrorsDb.LogError(ex);
        }
    }
}