﻿using UMTFunService.DAL;
using UMTFunService.Model;

namespace UMTFunService.BLL
{
    public class Events
    {
        public void CreateEvent(Event newEvent)
        {
            new EventsDb().CreateEvent(newEvent);
        }
    }
}