﻿using UMTFunService.DAL;
using UMTFunService.Model;

namespace UMTFunService.BLL
{
    public class Users
    {
        UsersDb usersDb;

        public Users()
        {
            usersDb = new UsersDb();
        }

        public void RegisterUser(User user)
        {
            usersDb.RegisterUser(user);
        }
    }
}