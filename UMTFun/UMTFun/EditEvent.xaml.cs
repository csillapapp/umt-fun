﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UMTFun
{
    /// <summary>
    /// Interaction logic for EditEvent.xaml
    /// </summary>
    public partial class EditEvent : Window
    {
        #region Members
        public static readonly DependencyProperty ImageUriProperty = DependencyProperty.Register("ImageUri", typeof(string), typeof(EditEvent));
        #endregion Members

        #region Properties
        public string ImageUri
        {
            get { return (string)GetValue(ImageUriProperty); }
            set { SetValue(ImageUriProperty, value); }
        }
        #endregion Properties

        #region Constructors
        public EditEvent()
        {
            InitializeComponent();
        }

        public EditEvent(string iconName)
        {
            InitializeComponent();
            DataContext = this;
            ImageUri = String.Format(@"\Images\{0}", iconName);
            LabelEventType.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            switch (iconName)
            {
                case "cookies.png":
                    LabelEventType.Content = "Cookies";
                    break;
                case "Custom Event.png":
                    LabelEventType.Content = "Custom Event";
                    break;
                case "DrinkBuilding.png":
                    LabelEventType.Content = "Drinkbuilding";
                    break;
                case "Food Order.png":
                    LabelEventType.Content = "Food Order";
                    break;
                case "football.png":
                    LabelEventType.Content = "Football";
                    break;
                case "TeamBuilding.png":
                    LabelEventType.Content = "Teambuilding";
                    break;
                default:
                    break;

            }
            SetDDLsValues();

        }

        private void SetDDLsValues()
        {
            DDLHoursDeadline.Items.Add("(hh)");
            DDLMinutesDeadline.Items.Add("(mm)");
            DDLHoursDeadline.SelectedIndex = 0;
            DDLMinutesDeadline.SelectedIndex = 0;
            for (int i = DateTime.Now.Hour; i < 25; i++)
            {
                DDLHoursDeadline.Items.Add(i);
            }

            for (int i = 1; i < 60; i++)
            {
                DDLMinutesDeadline.Items.Add(i);
            }
        }
        #endregion Constructors


    }
}
