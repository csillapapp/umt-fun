﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UMTFun
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCookies_Click(object sender, RoutedEventArgs e)
        {
            EditEvent cookieEvent = new EditEvent("cookies.png");
            cookieEvent.Show();
        }

        private void btnFoodOrder_Click(object sender, RoutedEventArgs e)
        {
            EditEvent foodEvent = new EditEvent("Food Order.png");
            foodEvent.Show();
        }

        private void btnTeambuilding_Click(object sender, RoutedEventArgs e)
        {
            EditEvent teambuildingEvent = new EditEvent("TeamBuilding.png");
            teambuildingEvent.Show();
        }

        private void btnDrinkbuilding_Click(object sender, RoutedEventArgs e)
        {
            EditEvent drinkbuildingEvent = new EditEvent("DrinkBuilding.png");
            drinkbuildingEvent.Show();
        }

        private void btnFootball_Click(object sender, RoutedEventArgs e)
        {
            EditEvent footballEvent = new EditEvent("football.png");
            footballEvent.Show();
        }

        private void btnCustomEvent_Click(object sender, RoutedEventArgs e)
        {
            EditEvent customEvent = new EditEvent("Custom Event.png");
            customEvent.Show();
        }

        
    }
}
